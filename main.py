import requests
from bs4 import BeautifulSoup

response = requests.get('https://www.pref.saitama.lg.jp/a0701/covid19/jokyo.html')
response.encoding = response.apparent_encoding
bs = BeautifulSoup(response.text, 'html.parser')
patients = []
tbody = bs.find_all('tbody')[2]
for tr in tbody.find_all('tr')[1:]:
    td = tr.find_all('td')
    tdtext = [t.text.strip() for t in td[1:6]]
    patients.insert(0,','.join(tdtext))
for i, patient in enumerate(patients):
    print(f"{i+1},{patient}")
