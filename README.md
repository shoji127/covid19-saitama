## 埼玉県のコロナ患者のサマリ情報の取得

## 実行方法
* pipenvを利用する場合  

``` python  
pipenv install
pipenv run python3 main.py
```

* pipenvを利用しない場合  

``` python  
pip install beautifulsoup4  
pip install requests  
python3 main.py
```

## 出力結果
``` bash
$ pipenv run python3 main.py | head
1,2/1,30代,男性,埼玉県外,退院
2,2/10,40代,男性,埼玉県,退院
3,2/14,-,-,埼玉県外,退院
4,2/21,未就学児,男性,埼玉県,退院
5,3/5,60代,男性,行田市,退院
6,3/5,50代,男性,上尾市,退院
7,3/6,60代,女性,行田市,退院
8,3/6,30代,女性,行田市,退院
9,3/8,40代,男性,富士見市,入院中
10,3/6,40代,女性,さいたま市,退院
```
